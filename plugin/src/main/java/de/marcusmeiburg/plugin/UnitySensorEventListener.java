package de.marcusmeiburg.plugin;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import com.unity3d.player.UnityPlayer;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UnitySensorEventListener implements SensorEventListener {
    private float[] lastValues;
    private final List<Sensor> sensors;

    private final Set<UnityCallback> unityCallbacks = new HashSet<>();

    public UnitySensorEventListener(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        lastValues = event.values.clone();
        for (UnityCallback unityCallback : unityCallbacks) {
            UnityPlayer.UnitySendMessage(unityCallback.getGameObjectName(),
                    unityCallback.getMethodName(), Arrays.toString(event.values));
        }
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public float[] getLastValues() {
        return lastValues;
    }

    public void addUnityCallback(UnityCallback callback) {
        synchronized (unityCallbacks) {
            unityCallbacks.add(callback);
        }
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    /**
     * Stores callback information
     */
    public static class UnityCallback {
        private final String gameObjectName;
        private final String methodName;

        UnityCallback(String gameObjectName, String methodName) {
            this.gameObjectName = gameObjectName;
            this.methodName = methodName;
        }

        public String getGameObjectName() {
            return gameObjectName;
        }

        public String getMethodName() {
            return methodName;
        }

        @Override
        public int hashCode() {
            return (gameObjectName + methodName).hashCode();
        }
    }
}