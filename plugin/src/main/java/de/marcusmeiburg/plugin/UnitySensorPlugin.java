package de.marcusmeiburg.plugin;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.hardware.Sensor.TYPE_ACCELEROMETER;
import static android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE;
import static android.hardware.Sensor.TYPE_GAME_ROTATION_VECTOR;
import static android.hardware.Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR;
import static android.hardware.Sensor.TYPE_GRAVITY;
import static android.hardware.Sensor.TYPE_GYROSCOPE;
import static android.hardware.Sensor.TYPE_GYROSCOPE_UNCALIBRATED;
import static android.hardware.Sensor.TYPE_HEART_RATE;
import static android.hardware.Sensor.TYPE_LIGHT;
import static android.hardware.Sensor.TYPE_LINEAR_ACCELERATION;
import static android.hardware.Sensor.TYPE_MAGNETIC_FIELD;
import static android.hardware.Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED;
import static android.hardware.Sensor.TYPE_PRESSURE;
import static android.hardware.Sensor.TYPE_PROXIMITY;
import static android.hardware.Sensor.TYPE_RELATIVE_HUMIDITY;
import static android.hardware.Sensor.TYPE_ROTATION_VECTOR;
import static android.hardware.Sensor.TYPE_SIGNIFICANT_MOTION;
import static android.hardware.Sensor.TYPE_STEP_COUNTER;
import static android.hardware.Sensor.TYPE_STEP_DETECTOR;

public class UnitySensorPlugin {

    private static final Object SINGLETON_LOCK = new Object();
    private static volatile UnitySensorPlugin instance;

    private int samplingPeriod = SensorManager.SENSOR_DELAY_GAME;
    private SensorManager sensorManager;
    private Map<String, UnitySensorEventListener> eventListeners = new HashMap<>();

    private UnitySensorPlugin() {}

    public static UnitySensorPlugin getInstance() {
        UnitySensorPlugin result = instance;
        if (instance == null) {
            synchronized (SINGLETON_LOCK) {
                result = instance;

                if (result == null) {
                    result = new UnitySensorPlugin();
                    result.sensorManager = (SensorManager) UnityPlayer.currentActivity.getSystemService(Context.SENSOR_SERVICE);
                    instance = result;
                }
            }
        }

        return result;
    }

    public void terminate() {
        Log.d(this.getClass().getName(), "Terminating UnitySensorPlugin.");
        for (SensorEventListener eventListener : eventListeners.values()) {
            sensorManager.unregisterListener(eventListener);
        }

        eventListeners.clear();
    }

    /**
     * Sets sensor sampling period in micro seconds.
     * Called by Unity
     *
     * @param samplingPeriodUs sampling period @see {@link SensorManager#registerListener(SensorEventListener, Sensor, int)}
     */
    public void setSamplingPeriod(int samplingPeriodUs) {
        samplingPeriod = samplingPeriodUs;
        for (UnitySensorEventListener eventListener : eventListeners.values()) {
            sensorManager.unregisterListener(eventListener);

            for (Sensor sensor : eventListener.getSensors()) {
                sensorManager.registerListener(eventListener, sensor, samplingPeriod);
            }
        }
    }

    /**
     * Obtains current sensor values
     * Called by Unity
     *
     * @param sensorKind sensor kind @see {@link Sensors}
     * @return latest sensor values
     */
    public float[] getSensorValues(String sensorKind) {
        UnitySensorEventListener sensorEventListener = eventListeners.get(convertSensorKind(sensorKind));
        if (sensorEventListener == null) {
            return null;
        }

        return sensorEventListener.getLastValues();
    }

    /**
     * Start to listen sensor
     * Called by Unity
     *
     * @param sensorKind sensor kind @see {@link Sensors}
     */
    public void startSensorListening(String sensorKind) {
        addSensorEventListener(sensorKind, null, null);
    }

    /**
     * Add sensor listener with callback
     * Called by Unity
     *
     * @param sensorKind sensor kind @see {@link Sensors}
     * @param gameObjectName Unity game object name
     * @param methodName Unity Behaviour's method name for callback
     */
    public void addSensorEventListener(String sensorKind, String gameObjectName, String methodName) {
        String sensorKindConverted = convertSensorKind(sensorKind);

        UnitySensorEventListener sensorEventListener = eventListeners.get(sensorKindConverted);

        if (sensorEventListener == null) {
            Sensors specifiedSensor;
            try {
                specifiedSensor = Sensors.valueOf(sensorKindConverted);
            } catch (IllegalArgumentException e) {
                // valueOf failed
                Log.e(getClass().getName(), "Bad sensor type: " + sensorKind + ", available types: " + Arrays.toString(Sensors.values()));
                return;
            }

            List<Sensor> sensors = sensorManager.getSensorList(specifiedSensor.getSensorType());
            sensorEventListener = new UnitySensorEventListener(sensors);
            for (Sensor sensor : sensors) {
                sensorManager.registerListener(sensorEventListener, sensor, samplingPeriod);
            }
            eventListeners.put(sensorKindConverted, sensorEventListener);
        }

        if (gameObjectName != null && methodName != null) {
            sensorEventListener.addUnityCallback(new UnitySensorEventListener.UnityCallback(gameObjectName, methodName));
        }
    }

    /**
     * Converts sensorKind to enum value string.
     *
     * @param sensorKind sensor kind string
     * @return string for enum
     */
    private String convertSensorKind(String sensorKind) {
        return sensorKind.toLowerCase(Locale.UK).replaceAll("_", "");
    }

    private enum Sensors {
        accelerometer(TYPE_ACCELEROMETER),
        ambienttemperature(TYPE_AMBIENT_TEMPERATURE),
        gamerotationvector(TYPE_GAME_ROTATION_VECTOR),
        geomagneticrotationvector(TYPE_GEOMAGNETIC_ROTATION_VECTOR),
        gravity(TYPE_GRAVITY),
        gyroscope(TYPE_GYROSCOPE),
        gyroscopeuncalibrated(TYPE_GYROSCOPE_UNCALIBRATED),
        heartrate(TYPE_HEART_RATE),
        light(TYPE_LIGHT),
        linearacceleration(TYPE_LINEAR_ACCELERATION),
        magneticfield(TYPE_MAGNETIC_FIELD),
        magneticfielduncalibrated(TYPE_MAGNETIC_FIELD_UNCALIBRATED),
        pressure(TYPE_PRESSURE),
        proximity(TYPE_PROXIMITY),
        relativehumidity(TYPE_RELATIVE_HUMIDITY),
        rotationvector(TYPE_ROTATION_VECTOR),
        significantmotion(TYPE_SIGNIFICANT_MOTION),
        stepcounter(TYPE_STEP_COUNTER),
        stepdetector(TYPE_STEP_DETECTOR),
        ;

        private int sensorType;

        Sensors(int sensorType) {
            this.sensorType = sensorType;
        }

        int getSensorType() {
            return sensorType;
        }
    }
}